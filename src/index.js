require("./modals/user");
require("./modals/Track");
const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const authRoutes = require("./routes/authRoutes");
const trackRoutes = require("./routes/trackRoutes");
const requireAuth = require("./Middlewares/requireAuth");

const app = express();

app.use(bodyParser.json());
app.use(authRoutes);
app.use(trackRoutes);
const DB =
  "mongodb+srv://root:root@mongodb.ayvwi.mongodb.net/mernDB?retryWrites=true&w=majority";

mongoose
  .connect(DB, {})
  .then((res) => {
    console.log("Connection Successfull");
  })
  .catch((err) => {
    console.log("no connection");
  });

const middlewares = (req, res, next) => {
  console.log("loading");
  next();
};
app.get("/", requireAuth, (req, res) => {
  res.send(`Your Email:${req.user.email}`);
});
app.get("/about", middlewares, (req, res) => {
  res.send(`Hello world from the server about`);
});
app.get("/contact", (req, res) => {
  res.send(`Hello world from the server contact`);
});
app.listen(3000, () => {
  console.log(`Server is running on port 3000`);
});
